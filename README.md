# Configurando o ambiente Centos7
Iniciamos Instalando no servidor __Ngnix__ + __Php__ + __MariaDB__ pelo seguinte comando
```bash
dnf install nginx mariadb-server php php-cli php-mysqlnd php-opcache php-xml php-gd php-soap php-bcmath php-intl php-mbstring php-json php-iconv php-fpm php-zip unzip git -y
```
__Vamos editar agora o arquivo php.ini__ 

  ``` vim /etc/php.ini```
```php
memory_limit =512M
upload_max_filesize = 200M
zlib.output_compression = On 
max_execution_time = 300 
date.timezone = America/Sao_Paulo	
```
Reiniciamos o nginx para aplicar as configurações

  ```  systemctl restart nginx ```

__Vamos então configurar o Banco de dados:__

Por padrão, o MariaDB não está protegido, então você precisará protegê-lo primeiro. Execute o seguinte script para garantir a segurança no MariaDB:

  ``` mysql_secure_installation  ``` 

Responda a todas as perguntas como mostrado abaixo:

> Enter current password for root (enter for none):
 
> Set root password? [Y/n] Y

> New password: 

> Re-enter new password: 

> Remove anonymous users? [Y/n] Y

> Disallow root login remotely? [Y/n] Y

> Remove test database and access to it? [Y/n] Y

> Reload privilege tables now? [Y/n] Y

Uma vez feito isso, faça login no MariaDB com o seguinte comando:

  ``` mysql -u root -p  ``` 

__Vamos criar agora dois banco de dados, um para o magento e o outro para o wordpress__


  ```
Magento
CREATE DATABASE magentodb;
GRANT ALL ON magentodb.* TO china@localhost IDENTIFIED BY 'passwordhere';

flush privileges;

exit;

CREATE DATABASE wordpressdb;
GRANT ALL ON wordpressdb.* TO china@localhost IDENTIFIED BY 'passwordhere';

flush privileges;

exit;

  ```
# Magento
Primeiro vamos criar o diretório e baixar o Magento  
```
mkdir /var/www/html/magento
cd /var/www/html/magento
wget https://github.com/magento/magento2/archive/2.3.zip
```
Extraia os arquivos para o diretório

```
unzip  2.3.zip
``` 
Em seguida, você precisará instalar o __Composer__, para instalar as dependências do PHP para Magento.
Você pode instalar o Composer com o seguinte comando:
```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer
composer -v
```
Rode o comando para instalar as dependências dentro do diretório:
```
composer update
composer install
```
Ao rodar o comando composer update
precisei habilitar algumas extensões na mão e mexer no swap.
```
executar comando :
/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
/sbin/mkswap /var/swap.1
/sbin/swapon /var/swap.1

habilitar extensão php:
sudo yum install php-bcmath --disablerepo=epel*
sudo yum install php-soap  --disablerepo=epel*
sudo yum install php-zip --disablerepo=epel*
```
Mude as permissões do diretório do Magento
```
chown -R nginx:nginx /var/www/html/magento
chmod -R 755 /var/www/html/magento
```
### Configurando PHP-FPM do Magento
Em seguida, você precisará configurar o pool PHP-FPM para a sua instância Magento. 

Você pode configurá-lo criando o seguinte arquivo vim ``` /etc/php-fpm.d/magento.conf:```
Adicione as seguintes linhas:

__Adicione as seguintes linhas:__
```
[magento]
user = nginx
group = nginx
listen.owner = nginx
listen.group = nginx
listen = /run/php-fpm/magento.sock
pm = ondemand
pm.max_children =  50
pm.process_idle_timeout = 10s
pm.max_requests = 500
chdir = /
```
Salve e feche o arquivo e reinicie o serviço __PHP-FPM__ para implementar as alterações:
```
systemctl restart php-fpm
```

### Configurando o Virtual Host do Magento
```
vim /etc/nginx/conf.d/magento.conf
```
Vamos adicionar as seguintes linhas
```
upstream fastcgi_backend {
  server   unix:/run/php-fpm/magento.sock;
}

server {
    listen 80;
    server_name loja.teikin.tk;

    set $MAGE_ROOT /var/www/html/magento;
    set $MAGE_MODE developer;

    access_log /var/log/nginx/magento-access.log;
    error_log /var/log/nginx/magento-error.log;

    include /var/www/html/magento/nginx.conf.sample;
}
```
Salve e feche o arquivo quando terminar. Em seguida, reinicie o serviço Nginx e PHP-FPM para implementar alterações:
```
systemctl restart nginx php-fpm
```
### Mudando a permissão do SELinux

Se aparecer está mensagem:
```
There are issue in Linux kernel security module (Security-Enhanced Linux (SELinux)) i.e SELinux permission issue.
```
Execute o comando:
```
chcon -R -t httpd_sys_rw_content_t /var/www/html/magento/
chmod -R a+w /var/www/html/magento/
```

Pronto, agora abre o navegador e termine de inserir os dados do banco de dados.


# Wordpress

Vamos primeiro criar o diretório do wordpress e baixar os arquivos do wordpress
```
mkdir /var/www/html/blog
cd /var/www/html/blog
wget https://wordpress.org/latest.tar.gz
```
Extrai o arquivo
```
tar -xvzf latest.tar.gz
```
Altere em seguida o usuário e o grupo do diretório
e as permissões também
```
chown -R nginx:nginx /var/www/html/blog
chmod -R 755 /var/www/html/blog
```

Verifique se o Selinux está habilitado , execute o seguinte comando para definir o contexto SElinux para o diretorio

```
/var/www/html/wordpress
sudo semanage fcontext -a -t httpd_sys_rw_content_t \
"/var/www/html/wordpress(/.*)?"
sudo restorecon -Rv /var/www/html/wordpress

```
Vamos criar agora o virtual host do wordpress
```
vim /etc/nginx/conf.d/blog.conf
```
```

server {
    listen 80;
    server_name blog.teikin.tk;

    root /var/www/html/blog;
    index index.php;

    access_log /var/log/nginx/blog-access.log;
    error_log /var/log/nginx/blog-error.log;
    
     location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/var/run/php-fpm.sock;
        fastcgi_index   index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }

}

```

Salve o arquivo e reinicie o Ngninx
```
systemctl restart nginx 
```
Pronto, abre o navegador e termine de configurar
o banco de dados com o site.



# Tomcat com proxy reverso

Primeiro passo, vamos intalar o Java
```
yum install -y java-11-openjdk-devel
java -version
```

Criamos o usuário __tomcat__
```
useradd -r tomcat
```
Em seguida vamos criar o diretório para fazer o donwload do tomcat
```
mkdir /usr/local/tomcat9
cd /usr/local/tomcat9
```

Dowload do tomcat
```
wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.tar.gz
```
Extair o arquivo
```
tar -xvf tomcat9.tar.gz
```
E então vamos mudar a permissão do usuário e grupo
do diretório para __tomcat__
```
chown -R tomcat.tomcat /usr/local/tomcat9
```

Vamos Definir variável de ambiente
Agora, podemos configurar a variável de ambiente CATALINA_HOME usando os seguintes comandos:

Essa variável é configurada para garantir que o acesso ao software seja permitido para todos os usuários do seu sistema.
```
echo "export CATALINA_HOME="/usr/local/tomcat9"" >> ~/.bashrc
source ~/.bashrc
```

Vamos adicionar as seguintes informações ao nosso novo arquivo de unidade.
```
vim /etc/systemd/system/tomcat.service
```
```
[Unit]
Description=Apache Tomcat Server
After=syslog.target network.target

[Service]
Type=forking
User=tomcat
Group=tomcat

Environment=CATALINA_PID=/usr/local/tomcat9/temp/tomcat.pid
Environment=CATALINA_HOME=/usr/local/tomcat9
Environment=CATALINA_BASE=/usr/local/tomcat9

ExecStart=/usr/local/tomcat9/bin/catalina.sh start
ExecStop=/usr/local/tomcat9/bin/catalina.sh stop

RestartSec=10
Restart=always
[Install]
WantedBy=multi-user.target

```
Salvamos o arquivo e em seguida  recarregamos o serviço para aplicar as alterações.
```
systemctl daemon-reload
```

Vamos iniciar o serviço do Tomcat e habilitá-lo.
```
systemctl start tomcat.service && systemctl enable tomcat.service
```

### Configurando o Nginx como um proxy reverso

Vamos criar o virtual host
```
 vim /etc/nginx/conf.d/tomcat.conf
```
E adicionamos as seguintes linhas

```
upstream tomcat {
 server 127.0.0.1:8080 weight=100 max_fails=5 fail_timeout=5;
 }
 
 server {
 listen 80;
 server_name tomcat.teikin.tk;
 
 location / {
 proxy_set_header X-Forwarded-Host $host;
 proxy_set_header X-Forwarded-Server $host;
 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 proxy_pass http://tomcat/;
 }
 }
```
pronto, basta acessar normalmente pelo navegador
```
tomcat.teikin.tk
```

# Site Simples

Primeiro vamos criar o diretório
```
mkdir /var/www/html/teikin.tk
```
Então criamos o virtual host do domínio
```
vim /etc/nginx/conf.d/site.conf
```
E adicionamos as linhas
```
server {
    server_name teikin.tk;
    root /var/www/html/teikin.tk;
    index index.php;

    access_log /var/log/nginx/site-access.log;
    error_log /var/log/nginx/site-error.log;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/var/run/php-fpm.sock;
        fastcgi_index   index.php;
        fastcgi_param SCRIPT_FILENAME 
        $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }
}
```
Salve o arquivo e lembre se de colocar no server_name o seu __domínio__

Crie algum arquivo ```index``` e jogue dentro do diretório __/var/www/html/teikin.tk__

Pronto, reinicia o __nginx__ e acesse seu domínio


# Configurando acesso https
Para isso vamos precisar instalar o __certbot__

Execute os seguintes comandos:
```
wget https://dl.eff.org/certbot-auto
 mv certbot-auto /usr/local/bin/certbot-auto
 chown root /usr/local/bin/certbot-auto
 chmod 0755 /usr/local/bin/certbot-auto


certbot --nginx -d teikin.tk

/usr/local/bin/certbot-auto --nginx -d teikin.tk

```

Agora Coloque os subdomínios para https

Execute o comando um por vez.
```
/usr/local/bin/certbot-auto --nginx -d loja.teikin.tk
```
```
/usr/local/bin/certbot-auto --nginx -d tomcat.teikin.tk
```
```
/usr/local/bin/certbot-auto --nginx -d blog.teikin.tk
```

Pronto! seus subdomínios estarão com __HTTPS__ nos sites.